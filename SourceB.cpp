#include "sqlite3.h"
#include <string>
#include <unordered_map>
#include <sstream>
#include <iostream>

std::unordered_map<std::string, std::vector<std::string>> results;

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			std::pair<std::string, std::vector<std::string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg) 
{
	int rc;
	std::stringstream ss;
	results.clear();

	ss << "select available, price from cars where id = '" << carid <<"';";
	rc = sqlite3_exec(db, ss.str().c_str(), callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}

	if (results["available"][0] == "0") return false;

	ss.clear();
	ss << "SELECT balance FROM accounts WHERE Buyer_id = " << buyerid << ";";
	rc = sqlite3_exec(db, ss.str().c_str(), callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}

	int price = std::stoi(results["price"][0]), balance = std::stoi(results["balance"][0]);
	if (price > balance) return false;

	ss.clear();
	ss << "UPDATE accounts SET balance = " << (balance - price) << " WHERE Buyer_id = " << buyerid << ";";
	rc = sqlite3_exec(db, ss.str().c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}

	ss.clear();
	ss << "UPDATE cars SET available = 0 WHERE id = " << carid << ";";

	rc = sqlite3_exec(db, ss.str().c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}

	return true;
}

bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	int rc;
	results.clear();
	std::stringstream ss;

	rc = sqlite3_exec(db, "BEGIN;", NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}

	ss << "select balance from accounts where id = '" << from << "';";
	rc = sqlite3_exec(db, ss.str().c_str(), callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}

	int srcBalance = std::stoi(results["balance"][0]);
	if (srcBalance < amount) return false;

	ss.clear();
	ss << "select balance from accounts where id = '" << to << "';";
	rc = sqlite3_exec(db, ss.str().c_str(), callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}
	int dstBalance = std::stoi(results["balance"][1]);

	ss.clear();
	ss << "UPDATE accounts SET balance = " <<  (srcBalance - amount) << " WHERE id = " << from << ";";

	rc = sqlite3_exec(db, ss.str().c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}

	ss.clear();
	ss << "UPDATE accounts SET balance = " << (dstBalance + amount) << " WHERE id = " << to << ";";

	rc = sqlite3_exec(db, ss.str().c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}

	rc = sqlite3_exec(db, "COMMIT;", NULL, 0, &zErrMsg);

	rc = sqlite3_exec(db, ss.str().c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}

	return true;
}

void whoCanBuy(int carId, sqlite3* db, char* zErrMsg)
{
	int rc;
	results.clear();
	std::stringstream ss;

	ss << "select price from cars where id = '" << carId << "';";
	rc = sqlite3_exec(db, ss.str().c_str(), callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return;
	}

	ss.clear();
	ss << "SELECT id WHERE balance >= " << results["price"][0] << ";";
	rc = sqlite3_exec(db, ss.str().c_str(), callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return;
	}
	std::cout << "ID of clients who are able to buy the car: " << std::endl;
	for (std::string id : results["id"])  std::cout << std::endl;
}

int main(void) 
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;
	
	// connection to the database
	rc = sqlite3_open("carsDealer.db", &db);
	
	if (rc)
	{
		std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	std::cout << carPurchase(30, 1, db, zErrMsg); // Fail
	std::cout << carPurchase(4, 23, db, zErrMsg); // Succ
	std::cout << carPurchase(12, 21, db, zErrMsg); // Succ

	std::cout << balanceTransfer(1, 2, 1, db, zErrMsg);
	
	rc = sqlite3_exec(db, "SELECT color AS Color, COUNT(*) AS AmountOfCars ORDER BY Model;", callback, 0, &zErrMsg);
	if (rc)
	{
		std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	system("Pause");
	return (0);
}